#!/bin/bash

cenarios=( 'uf20-01.cnf' 'uf100-01.cnf' 'uf250-01.cnf' )

python main.py -a SA -i 'uf20-01.cnf' -ti 100 -tf 0.001 -it 1000 -r 1 -e 10
echo 'SA uf20-01.cnf done'

python main.py -a BA -i 'uf20-01.cnf' -it 1000 -e 10
echo 'BA uf20-01.cnf done'


python main.py -a SA -i 'uf100-01.cnf' -ti 100 -tf 0.001 -it 100000 -r 1 -e 10
echo 'SA uf100-01.cnf done'

python main.py -a BA -i 'uf100-01.cnf' -it 100000 -e 10
echo 'BA uf100-01.cnf done'


python main.py -a SA -i 'uf250-01.cnf' -ti 100 -tf 0.001 -it 200000 -r 1 -e 10
echo 'SA uf250-01.cnf done'

python main.py -a BA -i 'uf250-01.cnf' -it 200000 -e 10
echo 'BA uf250-01.cnf done'
