#coding=utf-8
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')

from sa import SA
import matplotlib.pyplot as plt
import argparse

def main():
	iteracoes = args.iteracoes[0]
	if args.execucoes and args.execucoes[0] != 0:
		exe = args.execucoes[0]
	else:
		exe = 1

	arquivo_entrada = args.input[0]

	if args.alg[0] == 'BA' or args.alg[0] == 'ba':
		resultados = []
		media_melhor_energia = 0
		for i in range(1, exe+1):
			alg = SA(arquivo_entrada)
			lista_energia, clausulas, melhor_energia = alg.busca_aleatoria(iteracoes, i)
			media_melhor_energia += melhor_energia
			resultados.append(lista_energia)

		media_melhor_energia /= float(exe)

		media = []
		for j in range(0, len(resultados[0])-1):
			soma = 0
			for i in range(0, len(resultados)):
				soma += resultados[i][j]
			media.append(soma/len(resultados))

		print "------------------------------"
		print "Média de Cláusulas Satisfeitas: %.5f" % media_melhor_energia

		fig = plt.figure()
		plt.title('Busca Aleatória: Média %s' % arquivo_entrada)
		plt.xlim([0,len(resultados[0])])
		plt.ylim([0,clausulas])
		plt.xlabel('Iterações')
		plt.ylabel('Energia', color='b')
		plt.tick_params(axis="y", labelcolor="b")
		plt.plot(media, "b-", linewidth=1)
		fig.savefig('gráficos/BA_media_%s.pdf' % arquivo_entrada)



	elif args.alg[0] == 'SA' or args.alg[0] == 'sa':
		temperatura_inicial = args.t_inicial[0]
		temperatura_final = args.t_final[0]
		funcao_resfriamento = args.resfriamento[0]
		resultados = []
		media_melhor_energia = 0
		for i in range(1, exe+1):
			alg = SA(arquivo_entrada)
			lista_energia, clausulas, temperatura, melhor_energia = alg.recozimento(temperatura_inicial, temperatura_final, iteracoes, funcao_resfriamento, i)
			media_melhor_energia += melhor_energia
			resultados.append(lista_energia)

		media_melhor_energia /= float(exe)
		media = []
		for j in range(0, len(resultados[0])-1):
			soma = 0
			for i in range(0, len(resultados)):
				soma += resultados[i][j]
			media.append(soma/len(resultados))

		print "------------------------------"
		print "Média de Cláusulas Satisfeitas: %.5f" % media_melhor_energia

		fig = plt.figure()
		plt.title('Simulated Annealing: Média %s' % arquivo_entrada)
		plt.xlim([0,len(resultados[0])])
		plt.ylim([0,clausulas])
		plt.xlabel('Iterações')
		plt.ylabel('Energia', color='b')
		plt.tick_params(axis="y", labelcolor="b")
		plt.plot(media, "b-", linewidth=1)

		plt.twinx()
		plt.xlim([0,len(resultados[0])])
		plt.ylim([0,temperatura_inicial+1])
		plt.ylabel('Temperatura', color='r')
		plt.tick_params(axis="y", labelcolor="r")
		plt.plot(temperatura, "r-", linewidth=1)

		fig.savefig('gráficos/SA_media_%s.pdf' % arquivo_entrada)


	


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Simulated Annealing e Busca Aleatória')
	parser.add_argument('-a', dest='alg', action='store', nargs=1, help='Algoritmo para execução. SA|BA')
	parser.add_argument('-i', dest='input', action='store', nargs=1, help='Arquivo de Entrada')
	parser.add_argument('-ti', dest='t_inicial', action='store', nargs=1, type=float, help='Temperatura inicial')
	parser.add_argument('-tf', dest='t_final', action='store', nargs=1, type=float, help='Temperatura Final')
	parser.add_argument('-it', dest='iteracoes', action='store', nargs=1, type=int, help='Número de iterações')
	parser.add_argument('-r', dest='resfriamento', action='store', nargs=1, type=int, help='Função de Resfriamento: linear|sigmoid|exp|cos.')
	parser.add_argument('-e', dest='execucoes', action='store', nargs=1, type=int, help='Número de execuções')
	args = parser.parse_args()

	main()